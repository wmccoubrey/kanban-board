# kanban-board


## Starting the project

### Local with Lerna & Yarn Workspaces

```bash
yarn install # First time

yarn start
```

### Accessing the applications

- [UI](http://localhost:5000)
- [API](http://localhost:5001/api)
- [Swagger Docs](http://localhost:5001/api)

### Logging In

Project uses cognito for login and as it's a demo project anyone can sign up / login. When you open the UI, attemot to 
login with cognito and click the 'Sign Up' link

## Seeding the project

```bash
yarn seed
```