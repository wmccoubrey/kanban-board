#!/usr/bin/env sh

docker run --rm -v ${PWD}:/app -w /app node:14-alpine /bin/sh -c "$@"
