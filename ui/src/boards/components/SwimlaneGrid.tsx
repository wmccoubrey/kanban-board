import { Board } from '../../graphql-types';
import React from 'react';
import SwimlaneGridItem from './SwimlaneGridItem';
import styled from 'styled-components';

const StyledGrid = styled.div`
    height: 100%;
    display: grid;
    grid-auto-flow: column;
    /* should, in theory, work without overflow-x */
    overflow-x: scroll;
    justify-content: start;
`;

type SwimlaneGridProps = {
    board: Board;
};

const SwimlaneGrid = ({ board }: SwimlaneGridProps) => (
    <StyledGrid>
        {board.lanes.map((lane) => (
            <SwimlaneGridItem key={lane.id} swimlane={lane} />
        ))}
    </StyledGrid>
);

export default SwimlaneGrid;
