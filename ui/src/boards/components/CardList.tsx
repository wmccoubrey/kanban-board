import { Swimlane } from '../../graphql-types';
import { useMutation } from '@apollo/client';
import React from 'react';
import CardListItem from './CardListItem';
import { BOARD_QUERY, CARD_ORDER_MUTATION } from '../queries';
import styled from 'styled-components';

const StyledCardList = styled.ul`
    list-style: none;
    width: 100%;
`;

type CardListProps = {
    swimlane: Swimlane;
    triggerRefresh?: () => void | Promise<void>;
};

const CardList = ({ swimlane }: CardListProps) => {
    const [updateOrder] = useMutation(CARD_ORDER_MUTATION, {
        refetchQueries: [{ query: BOARD_QUERY, variables: { id: swimlane.boardId } }],
    });

    return (
        <StyledCardList>
            {swimlane.cards.map((card) => (
                <CardListItem
                    key={card.id}
                    card={card}
                    updateOrder={(cardId, order) => {
                        console.log('updating order', cardId, order);
                        return updateOrder({ variables: { id: cardId, order } });
                    }}
                />
            ))}
        </StyledCardList>
    );
};

export default CardList;
