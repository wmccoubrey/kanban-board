import { Swimlane } from '../../graphql-types';
import styled from 'styled-components';
import { Heading } from 'morse-react';
import React from 'react';
import CardList from './CardList';
import AddCardButton from './actions/AddCardButton';
import { useMutation } from '@apollo/client';
import { BOARD_QUERY, CARD_SWIMLANE_MUTATION } from '../queries';
import { useDrop } from 'react-dnd';
import { CardDragObject } from '../types';

const StyledGridItem = styled.div`
    scroll-snap-align: center;
    padding: calc(var(--gutter) / 2 * 1.5);
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
    background: #fff;
    border-radius: 8px;
    width: 23vw;
    height: 100%;
`;

type SwimlaneGridItemProps = {
    swimlane: Swimlane;
};

const SwimlaneGridItem = ({ swimlane }: SwimlaneGridItemProps) => {
    const [updateSwimlane] = useMutation(CARD_SWIMLANE_MUTATION, {
        refetchQueries: [{ query: BOARD_QUERY, variables: { id: swimlane.boardId } }],
    });

    const [, dropRef] = useDrop<CardDragObject, unknown, unknown>({
        accept: 'card',
        drop: async ({ card }) => {
            await updateSwimlane({ variables: { id: card.id, laneId: swimlane.id } });
        },
    });
    return (
        <StyledGridItem ref={dropRef}>
            <Heading level={Heading.Level.H4} content={swimlane.name} />
            <Heading level={Heading.Level.H4} content={`${swimlane.cardsAggregate.count?.id || 0} cards`} />

            <CardList swimlane={swimlane} />

            <AddCardButton swimlane={swimlane} />
        </StyledGridItem>
    );
};

export default SwimlaneGridItem;
