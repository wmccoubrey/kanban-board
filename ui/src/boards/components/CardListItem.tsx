import { Card } from '../../graphql-types';
import { useDrag } from 'react-dnd';
import React from 'react';
import { CardDragObject } from '../types';
import styled from 'styled-components';

const StyledCardListItem = styled.li`
    padding: 8px;
    margin: 10px;
    background-color: #fafafa;
    -webkit-box-shadow: 0px 0px 10px 0px #b1b1b1;
    box-shadow: 0px 0px 10px 0px #b1b1b1;
    min-height: 5vh;
    border-left: #454d5c 1px;
    border-left-style: groove;
`;

const StyledCardListItemHeading = styled.h5``;

type CardListItemProps = { card: Card; updateOrder: (cardId: number, order: number) => unknown };

const CardListItem = ({ card, updateOrder }: CardListItemProps) => {
    const [{ opacity }, dragRef] = useDrag<CardDragObject, undefined, { opacity: number }>({
        item: { type: 'card', card },
        collect: (monitor) => ({
            opacity: monitor.isDragging() ? 0.5 : 1,
        }),
        end: (item, monitor) => {
            if (!monitor.didDrop()) {
                return;
            }
            const source = monitor.getItem();
            const target = monitor.getDropResult();

            if (source.id === target.id) {
                return;
            }

            updateOrder(card.id, target.index);
        },
    });

    return (
        <StyledCardListItem ref={dragRef} style={{ opacity }}>
            <StyledCardListItemHeading>{card.title}</StyledCardListItemHeading>
        </StyledCardListItem>
    );
};

export default CardListItem;
