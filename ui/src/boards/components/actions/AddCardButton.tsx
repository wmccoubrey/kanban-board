import { Card, CreateCard, Swimlane } from '../../../graphql-types';
import React, { useCallback } from 'react';
import { Button, ButtonColor, ButtonType, Margin, Width } from 'morse-react';
import { useSingleInputDialog } from 'morse-react-dialogs';
import * as yup from 'yup';
import { useMutation } from '@apollo/client';
import { ADD_CARD_MUTATION, BOARD_QUERY } from '../../queries';

type AddCardButtonProps = {
    swimlane: Swimlane;
};

const AddCardButton = ({ swimlane }: AddCardButtonProps) => {
    const [addCard] = useMutation<Card, { card: CreateCard }>(ADD_CARD_MUTATION, {
        refetchQueries: [{ query: BOARD_QUERY, variables: { id: swimlane.boardId } }],
    });

    const onSubmit = useCallback(
        async (val) => {
            await addCard({ variables: { card: { boardId: swimlane.boardId, laneId: swimlane.id, title: val } } });
        },
        [addCard, swimlane],
    );

    const { show } = useSingleInputDialog({
        initialValue: '',
        title: 'Add new card',
        message: 'Enter card name',
        validationSchema: yup.string().required().min(5),
        submitButtonProps: { color: ButtonColor.Positive, content: 'Add Card' },
        cancelButtonProps: { color: ButtonColor.Negative, content: 'Cancel' },
        inputProps: { className: 'u-width-100pc' },
        onSubmit,
    });

    return (
        <Button
            type={ButtonType.Ghost}
            color={ButtonColor.Secondary}
            onClick={show}
            styleOptions={{ width: Width.Fill, margin: Margin.LeftRightLarge }}
        >
            Add Card
        </Button>
    );
};

export default AddCardButton;
