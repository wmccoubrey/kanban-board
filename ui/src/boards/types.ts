import { Card } from '../graphql-types';

export type CardDragObject = { type: 'card'; card: Card };
