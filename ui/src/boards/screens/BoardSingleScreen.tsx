import React from 'react';
import { useQuery } from '@apollo/client';
import { useParams } from 'react-router';
import { Redirect } from 'react-router-dom';
import { Board } from '../../graphql-types';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { BOARD_QUERY } from '../queries';
import SwimlaneGrid from '../components/SwimlaneGrid';

const BoardSingleScreen = () => {
    const { id } = useParams<{ id?: string }>();

    const { loading, error, data } = useQuery<{ board: Board }>(BOARD_QUERY, { variables: { id }, pollInterval: 5000 });

    if (!id) {
        return <Redirect to={'/boards'} />;
    }

    if (error) return <p>Error :(</p>;

    if (loading || !data) return <p>Loading...</p>;

    return (
        <DndProvider backend={HTML5Backend}>
            <SwimlaneGrid board={data.board} />
        </DndProvider>
    );
};

export default BoardSingleScreen;
