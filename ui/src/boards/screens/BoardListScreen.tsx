import React from 'react';
import { gql, useQuery } from '@apollo/client';
import { Link } from 'react-router-dom';
import {
    Easing,
    Embossed,
    GridLayout,
    Heading,
    Height,
    HorizontalAlignment,
    Padding,
    Rounded,
    Shadow,
    VerticalAlignment,
} from 'morse-react';
import { BoardConnection } from '../../graphql-types';

const BOARDS_QUERY = gql`
    query GetBoards {
        boards {
            edges {
                node {
                    id
                    name

                    lanes {
                        id
                        name
                        cardsAggregate {
                            count {
                                id
                            }
                        }
                    }
                }
            }
        }
    }
`;

const BoardListScreen = () => {
    const { loading, error, data } = useQuery<{ boards: BoardConnection }>(BOARDS_QUERY);

    if (error) return <p>Error :(</p>;

    if (loading) return <p>Loading...</p>;

    return (
        <div className="u-marg u-marg-left-right-large">
            <GridLayout columns={4}>
                {data?.boards.edges.map(({ node }) => (
                    <GridLayout.Item
                        key={node.id}
                        styleOptions={{
                            shadow: Shadow.Ambient,
                            height: Height.Half,
                            padding: Padding.Normal,
                            horizontalAlignment: HorizontalAlignment.Center,
                            verticalAlignment: VerticalAlignment.Middle,
                            easing: Easing.EaseInOut,
                            embossed: Embossed.EmbossedDeep,
                            rounded: Rounded.Large,
                        }}
                        // @ts-ignore
                        style={{ cursor: 'pointer' }}
                    >
                        <Link to={`/boards/${node.id}`}>
                            <Heading level={Heading.Level.H3}>{node.name}</Heading>

                            {node.lanes.map((lane) => (
                                <Heading
                                    key={lane.id}
                                    level={Heading.Level.H4}
                                    content={`${lane.name}: ${lane.cardsAggregate.count?.id || 0} cards`}
                                />
                            ))}
                        </Link>
                    </GridLayout.Item>
                ))}
            </GridLayout>
        </div>
    );
};

export default BoardListScreen;
