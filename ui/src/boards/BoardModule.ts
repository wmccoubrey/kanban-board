import { ContainerModule, interfaces } from 'inversify';
import { CORE_TYPES, PrimaryNavItem, Route } from '@gcdtech/acorn-react-core';
import BoardListScreen from './screens/BoardListScreen';
import BoardSingleScreen from './screens/BoardSingleScreen';

export default class BoardModule extends ContainerModule {
    constructor() {
        super((bind: interfaces.Bind) => {
            bind<Route>(CORE_TYPES.Route).toConstantValue({
                type: 'component',
                authenticated: true,
                path: '/boards',
                exact: true,
                component: BoardListScreen,
            });
            bind<Route>(CORE_TYPES.Route).toConstantValue({
                type: 'component',
                authenticated: true,
                path: '/boards/:id',
                exact: true,
                component: BoardSingleScreen,
            });
            bind<PrimaryNavItem>(CORE_TYPES.NavItem).toConstantValue({
                authenticated: false,
                text: 'Boards',
                to: '/boards',
            });
        });
    }
}
