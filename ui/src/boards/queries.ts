import { gql } from '@apollo/client';

export const BOARD_QUERY = gql`
    query GetBoard($id: ID!) {
        board(id: $id) {
            id
            name
            lanes(
                sorting: [{ field: order, direction: ASC }, { field: createdDate, direction: ASC }]
                paging: { offset: 0, limit: 50 }
            ) {
                id
                boardId
                name
                cardsAggregate {
                    count {
                        id
                    }
                }
                cards(sorting: { field: updatedDate, direction: DESC }) {
                    id
                    title
                }
            }
        }
    }
`;

export const CARD_SWIMLANE_MUTATION = gql`
    mutation UpdateSwimlane($id: ID!, $laneId: Int) {
        updateOneCard(input: { id: $id, update: { laneId: $laneId } }) {
            id
            laneId
        }
    }
`;

export const CARD_ORDER_MUTATION = gql`
    mutation UpdateSwimlane($id: ID!, $order: Int) {
        updateOneCard(input: { id: $id, update: { order: $order } }) {
            id
            order
        }
    }
`;

export const ADD_CARD_MUTATION = gql`
    mutation AddCard($card: CreateCard!) {
        createOneCard(input: { card: $card }) {
            title
        }
    }
`;
