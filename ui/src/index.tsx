import './boot';
import React from 'react';
import './styles.scss';
import { BrowserRouter, Switch, Link, Route as BrowserRoute } from 'react-router-dom';
import CenteredMessage from './core/components/CenteredMessage';
import { getInjectedRoutes } from '@gcdtech/acorn-react-core';
import StandardLayout from 'core/layouts/StandardLayout';
import { observer } from 'mobx-react-lite';
import AuthenticationStore from 'authentication/stores/AuthenticationStore';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { PortalContainer } from 'morse-react';
import { InitialisationContainer } from './core/components/InitialisationContainer';
import ReactDOM from 'react-dom';

const Loading = () => {
    return (
        <CenteredMessage
            icon="⏳"
            bigMessage="Welcome to Acorn"
            smallMessage="We're just getting a few things ready"
            showLoader
        />
    );
};

const Error = () => <CenteredMessage icon="⏳" bigMessage="Oops!" smallMessage="Something went wrong" />;

const App = observer(() => {
    const authStore = AuthenticationStore.get();

    const client = new ApolloClient({
        uri: 'http://localhost:5001/graphql',
        cache: new InMemoryCache(),
        headers: {
            authorization: authStore.token ? `Bearer ${authStore.token.token}` : '',
        },
    });
    return (
        <PortalContainer>
            <BrowserRouter>
                <Switch>
                    <ApolloProvider client={client}>
                        {getInjectedRoutes({
                            defaultLayout: StandardLayout,
                            userIsAuthenticated: authStore.isLoggedIn,
                            userPermissions: authStore.permissions,
                        })}
                    </ApolloProvider>
                    <BrowserRoute
                        key="404"
                        render={() => (
                            <CenteredMessage
                                icon="🔍"
                                bigMessage="This page doesn't exist"
                                smallMessage="Check the address, or you can return to the home page."
                            >
                                <Link to="/" className="c-button">
                                    Go home
                                </Link>
                            </CenteredMessage>
                        )}
                    />
                </Switch>
            </BrowserRouter>
        </PortalContainer>
    );
});

// Initial Render
ReactDOM.render(
    <InitialisationContainer LoadingScreen={Loading} ErrorScreen={Error} App={App} />,
    document.getElementById('root'),
);
