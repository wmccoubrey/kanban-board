export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
  /** Cursor for paging through collections */
  ConnectionCursor: any;
};

export type Swimlane = {
  __typename?: 'Swimlane';
  id: Scalars['Int'];
  name: Scalars['String'];
  boardId: Scalars['Int'];
  order: Scalars['Int'];
  createdDate: Scalars['DateTime'];
  updatedDate: Scalars['DateTime'];
  cards: Array<Card>;
  board: Board;
  cardsAggregate: SwimlaneCardsAggregateResponse;
};


export type SwimlaneCardsArgs = {
  paging?: Maybe<OffsetPaging>;
  filter?: Maybe<CardFilter>;
  sorting?: Maybe<Array<CardSort>>;
};


export type SwimlaneCardsAggregateArgs = {
  filter?: Maybe<CardAggregateFilter>;
};


export type OffsetPaging = {
  /** Limit the number of records returned */
  limit?: Maybe<Scalars['Int']>;
  /** Offset to start returning records from */
  offset?: Maybe<Scalars['Int']>;
};

export type CardFilter = {
  and?: Maybe<Array<CardFilter>>;
  or?: Maybe<Array<CardFilter>>;
  id?: Maybe<IntFieldComparison>;
  title?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  laneId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
  board?: Maybe<CardFilterBoardFilter>;
  lane?: Maybe<CardFilterSwimlaneFilter>;
};

export type IntFieldComparison = {
  is?: Maybe<Scalars['Boolean']>;
  isNot?: Maybe<Scalars['Boolean']>;
  eq?: Maybe<Scalars['Int']>;
  neq?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  gte?: Maybe<Scalars['Int']>;
  lt?: Maybe<Scalars['Int']>;
  lte?: Maybe<Scalars['Int']>;
  in?: Maybe<Array<Scalars['Int']>>;
  notIn?: Maybe<Array<Scalars['Int']>>;
  between?: Maybe<IntFieldComparisonBetween>;
  notBetween?: Maybe<IntFieldComparisonBetween>;
};

export type IntFieldComparisonBetween = {
  lower: Scalars['Int'];
  upper: Scalars['Int'];
};

export type StringFieldComparison = {
  is?: Maybe<Scalars['Boolean']>;
  isNot?: Maybe<Scalars['Boolean']>;
  eq?: Maybe<Scalars['String']>;
  neq?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  like?: Maybe<Scalars['String']>;
  notLike?: Maybe<Scalars['String']>;
  iLike?: Maybe<Scalars['String']>;
  notILike?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
};

export type DateFieldComparison = {
  is?: Maybe<Scalars['Boolean']>;
  isNot?: Maybe<Scalars['Boolean']>;
  eq?: Maybe<Scalars['DateTime']>;
  neq?: Maybe<Scalars['DateTime']>;
  gt?: Maybe<Scalars['DateTime']>;
  gte?: Maybe<Scalars['DateTime']>;
  lt?: Maybe<Scalars['DateTime']>;
  lte?: Maybe<Scalars['DateTime']>;
  in?: Maybe<Array<Scalars['DateTime']>>;
  notIn?: Maybe<Array<Scalars['DateTime']>>;
  between?: Maybe<DateFieldComparisonBetween>;
  notBetween?: Maybe<DateFieldComparisonBetween>;
};

export type DateFieldComparisonBetween = {
  lower: Scalars['DateTime'];
  upper: Scalars['DateTime'];
};

export type CardFilterBoardFilter = {
  and?: Maybe<Array<CardFilterBoardFilter>>;
  or?: Maybe<Array<CardFilterBoardFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  description?: Maybe<StringFieldComparison>;
};

export type CardFilterSwimlaneFilter = {
  and?: Maybe<Array<CardFilterSwimlaneFilter>>;
  or?: Maybe<Array<CardFilterSwimlaneFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type CardSort = {
  field: CardSortFields;
  direction: SortDirection;
  nulls?: Maybe<SortNulls>;
};

export enum CardSortFields {
  Id = 'id',
  Title = 'title',
  BoardId = 'boardId',
  LaneId = 'laneId',
  Order = 'order',
  CreatedDate = 'createdDate',
  UpdatedDate = 'updatedDate'
}

/** Sort Directions */
export enum SortDirection {
  Asc = 'ASC',
  Desc = 'DESC'
}

/** Sort Nulls Options */
export enum SortNulls {
  NullsFirst = 'NULLS_FIRST',
  NullsLast = 'NULLS_LAST'
}

export type CardAggregateFilter = {
  and?: Maybe<Array<CardAggregateFilter>>;
  or?: Maybe<Array<CardAggregateFilter>>;
  id?: Maybe<IntFieldComparison>;
  title?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  laneId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type Card = {
  __typename?: 'Card';
  id: Scalars['Int'];
  title: Scalars['String'];
  boardId: Scalars['Int'];
  laneId: Scalars['Int'];
  order: Scalars['Int'];
  createdDate: Scalars['DateTime'];
  updatedDate: Scalars['DateTime'];
  board: Board;
  lane: Swimlane;
};

export type Board = {
  __typename?: 'Board';
  id: Scalars['Int'];
  name: Scalars['String'];
  description: Scalars['String'];
  createdDate: Scalars['DateTime'];
  updatedDate: Scalars['DateTime'];
  cards: Array<Card>;
  lanes: Array<Swimlane>;
  cardsAggregate: BoardCardsAggregateResponse;
  lanesAggregate: BoardLanesAggregateResponse;
};


export type BoardCardsArgs = {
  paging?: Maybe<OffsetPaging>;
  filter?: Maybe<CardFilter>;
  sorting?: Maybe<Array<CardSort>>;
};


export type BoardLanesArgs = {
  paging?: Maybe<OffsetPaging>;
  filter?: Maybe<SwimlaneFilter>;
  sorting?: Maybe<Array<SwimlaneSort>>;
};


export type BoardCardsAggregateArgs = {
  filter?: Maybe<CardAggregateFilter>;
};


export type BoardLanesAggregateArgs = {
  filter?: Maybe<SwimlaneAggregateFilter>;
};

export type SwimlaneFilter = {
  and?: Maybe<Array<SwimlaneFilter>>;
  or?: Maybe<Array<SwimlaneFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
  board?: Maybe<SwimlaneFilterBoardFilter>;
  cards?: Maybe<SwimlaneFilterCardFilter>;
};

export type SwimlaneFilterBoardFilter = {
  and?: Maybe<Array<SwimlaneFilterBoardFilter>>;
  or?: Maybe<Array<SwimlaneFilterBoardFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  description?: Maybe<StringFieldComparison>;
};

export type SwimlaneFilterCardFilter = {
  and?: Maybe<Array<SwimlaneFilterCardFilter>>;
  or?: Maybe<Array<SwimlaneFilterCardFilter>>;
  id?: Maybe<IntFieldComparison>;
  title?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  laneId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type SwimlaneSort = {
  field: SwimlaneSortFields;
  direction: SortDirection;
  nulls?: Maybe<SortNulls>;
};

export enum SwimlaneSortFields {
  Id = 'id',
  Name = 'name',
  BoardId = 'boardId',
  Order = 'order',
  CreatedDate = 'createdDate',
  UpdatedDate = 'updatedDate'
}

export type SwimlaneAggregateFilter = {
  and?: Maybe<Array<SwimlaneAggregateFilter>>;
  or?: Maybe<Array<SwimlaneAggregateFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type DeleteManyResponse = {
  __typename?: 'DeleteManyResponse';
  /** The number of records deleted. */
  deletedCount: Scalars['Int'];
};

export type BoardDeleteResponse = {
  __typename?: 'BoardDeleteResponse';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type UpdateManyResponse = {
  __typename?: 'UpdateManyResponse';
  /** The number of records updated. */
  updatedCount: Scalars['Int'];
};

export type BoardEdge = {
  __typename?: 'BoardEdge';
  /** The node containing the Board */
  node: Board;
  /** Cursor for this node. */
  cursor: Scalars['ConnectionCursor'];
};


export type PageInfo = {
  __typename?: 'PageInfo';
  /** true if paging forward and there are more records. */
  hasNextPage?: Maybe<Scalars['Boolean']>;
  /** true if paging backwards and there are more records. */
  hasPreviousPage?: Maybe<Scalars['Boolean']>;
  /** The cursor of the first returned record. */
  startCursor?: Maybe<Scalars['ConnectionCursor']>;
  /** The cursor of the last returned record. */
  endCursor?: Maybe<Scalars['ConnectionCursor']>;
};

export type BoardConnection = {
  __typename?: 'BoardConnection';
  /** Paging information */
  pageInfo: PageInfo;
  /** Array of edges. */
  edges: Array<BoardEdge>;
};

export type BoardCountAggregate = {
  __typename?: 'BoardCountAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['Int']>;
  description?: Maybe<Scalars['Int']>;
};

export type BoardSumAggregate = {
  __typename?: 'BoardSumAggregate';
  id?: Maybe<Scalars['Float']>;
};

export type BoardAvgAggregate = {
  __typename?: 'BoardAvgAggregate';
  id?: Maybe<Scalars['Float']>;
};

export type BoardMinAggregate = {
  __typename?: 'BoardMinAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
};

export type BoardMaxAggregate = {
  __typename?: 'BoardMaxAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
};

export type BoardAggregateResponse = {
  __typename?: 'BoardAggregateResponse';
  count?: Maybe<BoardCountAggregate>;
  sum?: Maybe<BoardSumAggregate>;
  avg?: Maybe<BoardAvgAggregate>;
  min?: Maybe<BoardMinAggregate>;
  max?: Maybe<BoardMaxAggregate>;
};

export type BoardCardsCountAggregate = {
  __typename?: 'BoardCardsCountAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['Int']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['Int']>;
  updatedDate?: Maybe<Scalars['Int']>;
};

export type BoardCardsSumAggregate = {
  __typename?: 'BoardCardsSumAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  laneId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type BoardCardsAvgAggregate = {
  __typename?: 'BoardCardsAvgAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  laneId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type BoardCardsMinAggregate = {
  __typename?: 'BoardCardsMinAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type BoardCardsMaxAggregate = {
  __typename?: 'BoardCardsMaxAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type BoardCardsAggregateResponse = {
  __typename?: 'BoardCardsAggregateResponse';
  count?: Maybe<BoardCardsCountAggregate>;
  sum?: Maybe<BoardCardsSumAggregate>;
  avg?: Maybe<BoardCardsAvgAggregate>;
  min?: Maybe<BoardCardsMinAggregate>;
  max?: Maybe<BoardCardsMaxAggregate>;
};

export type BoardLanesCountAggregate = {
  __typename?: 'BoardLanesCountAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['Int']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['Int']>;
  updatedDate?: Maybe<Scalars['Int']>;
};

export type BoardLanesSumAggregate = {
  __typename?: 'BoardLanesSumAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type BoardLanesAvgAggregate = {
  __typename?: 'BoardLanesAvgAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type BoardLanesMinAggregate = {
  __typename?: 'BoardLanesMinAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type BoardLanesMaxAggregate = {
  __typename?: 'BoardLanesMaxAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type BoardLanesAggregateResponse = {
  __typename?: 'BoardLanesAggregateResponse';
  count?: Maybe<BoardLanesCountAggregate>;
  sum?: Maybe<BoardLanesSumAggregate>;
  avg?: Maybe<BoardLanesAvgAggregate>;
  min?: Maybe<BoardLanesMinAggregate>;
  max?: Maybe<BoardLanesMaxAggregate>;
};

export type SwimlaneDeleteResponse = {
  __typename?: 'SwimlaneDeleteResponse';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type SwimlaneEdge = {
  __typename?: 'SwimlaneEdge';
  /** The node containing the Swimlane */
  node: Swimlane;
  /** Cursor for this node. */
  cursor: Scalars['ConnectionCursor'];
};

export type SwimlaneConnection = {
  __typename?: 'SwimlaneConnection';
  /** Paging information */
  pageInfo: PageInfo;
  /** Array of edges. */
  edges: Array<SwimlaneEdge>;
};

export type SwimlaneCountAggregate = {
  __typename?: 'SwimlaneCountAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['Int']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['Int']>;
  updatedDate?: Maybe<Scalars['Int']>;
};

export type SwimlaneSumAggregate = {
  __typename?: 'SwimlaneSumAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type SwimlaneAvgAggregate = {
  __typename?: 'SwimlaneAvgAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type SwimlaneMinAggregate = {
  __typename?: 'SwimlaneMinAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type SwimlaneMaxAggregate = {
  __typename?: 'SwimlaneMaxAggregate';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type SwimlaneAggregateResponse = {
  __typename?: 'SwimlaneAggregateResponse';
  count?: Maybe<SwimlaneCountAggregate>;
  sum?: Maybe<SwimlaneSumAggregate>;
  avg?: Maybe<SwimlaneAvgAggregate>;
  min?: Maybe<SwimlaneMinAggregate>;
  max?: Maybe<SwimlaneMaxAggregate>;
};

export type SwimlaneCardsCountAggregate = {
  __typename?: 'SwimlaneCardsCountAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['Int']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['Int']>;
  updatedDate?: Maybe<Scalars['Int']>;
};

export type SwimlaneCardsSumAggregate = {
  __typename?: 'SwimlaneCardsSumAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  laneId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type SwimlaneCardsAvgAggregate = {
  __typename?: 'SwimlaneCardsAvgAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  laneId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type SwimlaneCardsMinAggregate = {
  __typename?: 'SwimlaneCardsMinAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type SwimlaneCardsMaxAggregate = {
  __typename?: 'SwimlaneCardsMaxAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type SwimlaneCardsAggregateResponse = {
  __typename?: 'SwimlaneCardsAggregateResponse';
  count?: Maybe<SwimlaneCardsCountAggregate>;
  sum?: Maybe<SwimlaneCardsSumAggregate>;
  avg?: Maybe<SwimlaneCardsAvgAggregate>;
  min?: Maybe<SwimlaneCardsMinAggregate>;
  max?: Maybe<SwimlaneCardsMaxAggregate>;
};

export type CardDeleteResponse = {
  __typename?: 'CardDeleteResponse';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type CardEdge = {
  __typename?: 'CardEdge';
  /** The node containing the Card */
  node: Card;
  /** Cursor for this node. */
  cursor: Scalars['ConnectionCursor'];
};

export type CardConnection = {
  __typename?: 'CardConnection';
  /** Paging information */
  pageInfo: PageInfo;
  /** Array of edges. */
  edges: Array<CardEdge>;
};

export type CardCountAggregate = {
  __typename?: 'CardCountAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['Int']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['Int']>;
  updatedDate?: Maybe<Scalars['Int']>;
};

export type CardSumAggregate = {
  __typename?: 'CardSumAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  laneId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type CardAvgAggregate = {
  __typename?: 'CardAvgAggregate';
  id?: Maybe<Scalars['Float']>;
  boardId?: Maybe<Scalars['Float']>;
  laneId?: Maybe<Scalars['Float']>;
  order?: Maybe<Scalars['Float']>;
};

export type CardMinAggregate = {
  __typename?: 'CardMinAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type CardMaxAggregate = {
  __typename?: 'CardMaxAggregate';
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type CardAggregateResponse = {
  __typename?: 'CardAggregateResponse';
  count?: Maybe<CardCountAggregate>;
  sum?: Maybe<CardSumAggregate>;
  avg?: Maybe<CardAvgAggregate>;
  min?: Maybe<CardMinAggregate>;
  max?: Maybe<CardMaxAggregate>;
};

export type Query = {
  __typename?: 'Query';
  board?: Maybe<Board>;
  boards: BoardConnection;
  boardAggregate: BoardAggregateResponse;
  swimlane?: Maybe<Swimlane>;
  swimlanes: SwimlaneConnection;
  swimlaneAggregate: SwimlaneAggregateResponse;
  card?: Maybe<Card>;
  cards: CardConnection;
  cardAggregate: CardAggregateResponse;
};


export type QueryBoardArgs = {
  id: Scalars['ID'];
};


export type QueryBoardsArgs = {
  paging?: Maybe<CursorPaging>;
  filter?: Maybe<BoardFilter>;
  sorting?: Maybe<Array<BoardSort>>;
};


export type QueryBoardAggregateArgs = {
  filter?: Maybe<BoardAggregateFilter>;
};


export type QuerySwimlaneArgs = {
  id: Scalars['ID'];
};


export type QuerySwimlanesArgs = {
  paging?: Maybe<CursorPaging>;
  filter?: Maybe<SwimlaneFilter>;
  sorting?: Maybe<Array<SwimlaneSort>>;
};


export type QuerySwimlaneAggregateArgs = {
  filter?: Maybe<SwimlaneAggregateFilter>;
};


export type QueryCardArgs = {
  id: Scalars['ID'];
};


export type QueryCardsArgs = {
  paging?: Maybe<CursorPaging>;
  filter?: Maybe<CardFilter>;
  sorting?: Maybe<Array<CardSort>>;
};


export type QueryCardAggregateArgs = {
  filter?: Maybe<CardAggregateFilter>;
};

export type CursorPaging = {
  /** Paginate before opaque cursor */
  before?: Maybe<Scalars['ConnectionCursor']>;
  /** Paginate after opaque cursor */
  after?: Maybe<Scalars['ConnectionCursor']>;
  /** Paginate first */
  first?: Maybe<Scalars['Int']>;
  /** Paginate last */
  last?: Maybe<Scalars['Int']>;
};

export type BoardFilter = {
  and?: Maybe<Array<BoardFilter>>;
  or?: Maybe<Array<BoardFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  description?: Maybe<StringFieldComparison>;
  cards?: Maybe<BoardFilterCardFilter>;
  lanes?: Maybe<BoardFilterSwimlaneFilter>;
};

export type BoardFilterCardFilter = {
  and?: Maybe<Array<BoardFilterCardFilter>>;
  or?: Maybe<Array<BoardFilterCardFilter>>;
  id?: Maybe<IntFieldComparison>;
  title?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  laneId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type BoardFilterSwimlaneFilter = {
  and?: Maybe<Array<BoardFilterSwimlaneFilter>>;
  or?: Maybe<Array<BoardFilterSwimlaneFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type BoardSort = {
  field: BoardSortFields;
  direction: SortDirection;
  nulls?: Maybe<SortNulls>;
};

export enum BoardSortFields {
  Id = 'id',
  Name = 'name',
  Description = 'description'
}

export type BoardAggregateFilter = {
  and?: Maybe<Array<BoardAggregateFilter>>;
  or?: Maybe<Array<BoardAggregateFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  description?: Maybe<StringFieldComparison>;
};

export type Mutation = {
  __typename?: 'Mutation';
  deleteOneBoard: BoardDeleteResponse;
  deleteManyBoards: DeleteManyResponse;
  updateOneBoard: Board;
  updateManyBoards: UpdateManyResponse;
  createOneBoard: Board;
  createManyBoards: Array<Board>;
  deleteOneSwimlane: SwimlaneDeleteResponse;
  deleteManySwimlanes: DeleteManyResponse;
  updateOneSwimlane: Swimlane;
  updateManySwimlanes: UpdateManyResponse;
  createOneSwimlane: Swimlane;
  createManySwimlanes: Array<Swimlane>;
  deleteOneCard: CardDeleteResponse;
  deleteManyCards: DeleteManyResponse;
  updateOneCard: Card;
  updateManyCards: UpdateManyResponse;
  createOneCard: Card;
  createManyCards: Array<Card>;
};


export type MutationDeleteOneBoardArgs = {
  input: DeleteOneInput;
};


export type MutationDeleteManyBoardsArgs = {
  input: DeleteManyBoardsInput;
};


export type MutationUpdateOneBoardArgs = {
  input: UpdateOneBoardInput;
};


export type MutationUpdateManyBoardsArgs = {
  input: UpdateManyBoardsInput;
};


export type MutationCreateOneBoardArgs = {
  input: CreateOneBoardInput;
};


export type MutationCreateManyBoardsArgs = {
  input: CreateManyBoardsInput;
};


export type MutationDeleteOneSwimlaneArgs = {
  input: DeleteOneInput;
};


export type MutationDeleteManySwimlanesArgs = {
  input: DeleteManySwimlanesInput;
};


export type MutationUpdateOneSwimlaneArgs = {
  input: UpdateOneSwimlaneInput;
};


export type MutationUpdateManySwimlanesArgs = {
  input: UpdateManySwimlanesInput;
};


export type MutationCreateOneSwimlaneArgs = {
  input: CreateOneSwimlaneInput;
};


export type MutationCreateManySwimlanesArgs = {
  input: CreateManySwimlanesInput;
};


export type MutationDeleteOneCardArgs = {
  input: DeleteOneInput;
};


export type MutationDeleteManyCardsArgs = {
  input: DeleteManyCardsInput;
};


export type MutationUpdateOneCardArgs = {
  input: UpdateOneCardInput;
};


export type MutationUpdateManyCardsArgs = {
  input: UpdateManyCardsInput;
};


export type MutationCreateOneCardArgs = {
  input: CreateOneCardInput;
};


export type MutationCreateManyCardsArgs = {
  input: CreateManyCardsInput;
};

export type DeleteOneInput = {
  /** The id of the record to delete. */
  id: Scalars['ID'];
};

export type DeleteManyBoardsInput = {
  /** Filter to find records to delete */
  filter: BoardDeleteFilter;
};

export type BoardDeleteFilter = {
  and?: Maybe<Array<BoardDeleteFilter>>;
  or?: Maybe<Array<BoardDeleteFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  description?: Maybe<StringFieldComparison>;
};

export type UpdateOneBoardInput = {
  /** The id of the record to update */
  id: Scalars['ID'];
  /** The update to apply. */
  update: UpdateBoard;
};

export type UpdateBoard = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type UpdateManyBoardsInput = {
  /** Filter used to find fields to update */
  filter: BoardUpdateFilter;
  /** The update to apply to all records found using the filter */
  update: UpdateBoard;
};

export type BoardUpdateFilter = {
  and?: Maybe<Array<BoardUpdateFilter>>;
  or?: Maybe<Array<BoardUpdateFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  description?: Maybe<StringFieldComparison>;
};

export type CreateOneBoardInput = {
  /** The record to create */
  board: CreateBoard;
};

export type CreateBoard = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type CreateManyBoardsInput = {
  /** Array of records to create */
  boards: Array<CreateBoard>;
};

export type DeleteManySwimlanesInput = {
  /** Filter to find records to delete */
  filter: SwimlaneDeleteFilter;
};

export type SwimlaneDeleteFilter = {
  and?: Maybe<Array<SwimlaneDeleteFilter>>;
  or?: Maybe<Array<SwimlaneDeleteFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type UpdateOneSwimlaneInput = {
  /** The id of the record to update */
  id: Scalars['ID'];
  /** The update to apply. */
  update: UpdateSwimlane;
};

export type UpdateSwimlane = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type UpdateManySwimlanesInput = {
  /** Filter used to find fields to update */
  filter: SwimlaneUpdateFilter;
  /** The update to apply to all records found using the filter */
  update: UpdateSwimlane;
};

export type SwimlaneUpdateFilter = {
  and?: Maybe<Array<SwimlaneUpdateFilter>>;
  or?: Maybe<Array<SwimlaneUpdateFilter>>;
  id?: Maybe<IntFieldComparison>;
  name?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type CreateOneSwimlaneInput = {
  /** The record to create */
  swimlane: CreateSwimlane;
};

export type CreateSwimlane = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type CreateManySwimlanesInput = {
  /** Array of records to create */
  swimlanes: Array<CreateSwimlane>;
};

export type DeleteManyCardsInput = {
  /** Filter to find records to delete */
  filter: CardDeleteFilter;
};

export type CardDeleteFilter = {
  and?: Maybe<Array<CardDeleteFilter>>;
  or?: Maybe<Array<CardDeleteFilter>>;
  id?: Maybe<IntFieldComparison>;
  title?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  laneId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type UpdateOneCardInput = {
  /** The id of the record to update */
  id: Scalars['ID'];
  /** The update to apply. */
  update: UpdateCard;
};

export type UpdateCard = {
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type UpdateManyCardsInput = {
  /** Filter used to find fields to update */
  filter: CardUpdateFilter;
  /** The update to apply to all records found using the filter */
  update: UpdateCard;
};

export type CardUpdateFilter = {
  and?: Maybe<Array<CardUpdateFilter>>;
  or?: Maybe<Array<CardUpdateFilter>>;
  id?: Maybe<IntFieldComparison>;
  title?: Maybe<StringFieldComparison>;
  boardId?: Maybe<IntFieldComparison>;
  laneId?: Maybe<IntFieldComparison>;
  order?: Maybe<IntFieldComparison>;
  createdDate?: Maybe<DateFieldComparison>;
  updatedDate?: Maybe<DateFieldComparison>;
};

export type CreateOneCardInput = {
  /** The record to create */
  card: CreateCard;
};

export type CreateCard = {
  id?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  boardId?: Maybe<Scalars['Int']>;
  laneId?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['Int']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  updatedDate?: Maybe<Scalars['DateTime']>;
};

export type CreateManyCardsInput = {
  /** Array of records to create */
  cards: Array<CreateCard>;
};
