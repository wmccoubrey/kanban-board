export const AUTH_TYPES = {
    AuthenticationService: Symbol.for("AuthenticationService"),
    TokenStorageKey: Symbol.for("TokenStorageKey"),
};
