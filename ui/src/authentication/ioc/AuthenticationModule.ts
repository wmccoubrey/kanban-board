import { CORE_TYPES, Route } from "@gcdtech/acorn-react-core";
import LogoutScreen from "authentication/logout/screens/LogoutScreen";
import LogoutUseCase from "authentication/logout/use-cases/LogoutUseCase";
import LoginScreen from "authentication/screens/LoginScreen";
import NotPermittedScreen from "authentication/screens/NotPermittedScreen";
import AuthenticationService from "authentication/services/AuthenticationService";
import TokenAuthenticationService from "authentication/services/TokenAuthenticationService";
import DoPostLoginSetupUseCase from "authentication/use-cases/DoPostLoginSetupUseCase";
import SetAuthenticationTokenFromStorageUseCase from "authentication/use-cases/SetAuthenticationTokenFromStorageUseCase";
import { ContainerModule, interfaces } from "inversify";
import { AUTH_TYPES } from "./AuthenticationTypes";
import LoginLayout from "authentication/layout/LoginLayout";

export default class AuthenticationModule extends ContainerModule {
    constructor() {
        super((bind: interfaces.Bind) => {
            /* ROUTES */
            bind<Route>(CORE_TYPES.Route).toConstantValue({
                type: "component",
                authenticated: false,
                exact: false,
                path: "/auth/login",
                component: LoginScreen,
                layoutOverride: LoginLayout,
                layoutProps: { pageTitle: "Login" },
            });
            bind<Route>(CORE_TYPES.Route).toConstantValue({
                type: "component",
                authenticated: false,
                exact: true,
                path: "/auth/logout",
                component: LogoutScreen,
                layoutOverride: LoginLayout,
                layoutProps: { pageTitle: "Logged Out" },
            });
            bind<Route>(CORE_TYPES.Route).toConstantValue({
                type: "component",
                authenticated: false,
                exact: true,
                path: "/auth/not-permitted",
                component: NotPermittedScreen,
                renderLayout: false,
            });

            /* INIT */
            bind<AuthenticationService>(AUTH_TYPES.AuthenticationService).to(
                TokenAuthenticationService
            );
            bind(AUTH_TYPES.TokenStorageKey).toConstantValue("ApiAuthenticationToken");
            bind(CORE_TYPES.InitialiseUseCase).to(SetAuthenticationTokenFromStorageUseCase);
            bind(CORE_TYPES.InitialiseUseCase).to(DoPostLoginSetupUseCase);
            bind(CORE_TYPES.LogoutUseCase).to(LogoutUseCase);
            bind(CORE_TYPES.ForbiddenUseCase).to(LogoutUseCase);
        });
    }
}
