import AuthenticationToken from "authentication/entities/AuthenticationToken";

export default interface AuthenticationService {
    getLoginUrl(): Promise<string>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    login(payload: any): AuthenticationToken | Promise<AuthenticationToken>;
}
