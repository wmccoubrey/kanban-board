import { injectable, inject } from "inversify";
import AuthenticationService from "./AuthenticationService";
import { CORE_TYPES } from "@gcdtech/acorn-react-core";
import { ApiAdapter } from "@gcdtech/acorn-react-core";
import { ApiThrowsError } from "@gcdtech/acorn-react-core";
import AuthenticationToken from "authentication/entities/AuthenticationToken";

@injectable()
export default class TokenAuthenticationService implements AuthenticationService {
    @inject(CORE_TYPES.ApiAdapter) protected apiAdapter: ApiAdapter;

    protected endpointUrl = "/auth";

    @ApiThrowsError
    async getLoginUrl(): Promise<string> {
        const url = this.endpointUrl + `/login?redirectUrl=${window.location.href}`;

        const response = await this.apiAdapter.get<string>(url);

        return response.data;
    }

    @ApiThrowsError
    async login(payload: never): Promise<AuthenticationToken> {
        const url = this.endpointUrl + "/login";

        const response = await this.apiAdapter.post<{ token: string }>(url, payload);

        const authentication = new AuthenticationToken();
        authentication.token = response.data.token;

        return authentication;
    }
}
