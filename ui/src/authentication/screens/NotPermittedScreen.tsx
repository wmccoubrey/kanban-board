import CenteredMessage from "core/components/CenteredMessage";
import React from "react";

const NotPermittedScreen = () => {
    return (
        <CenteredMessage
            icon="🚷"
            bigMessage="You do not have permissions to view this page"
            smallMessage="You may have got to this page by accident. Try again, or contact us if you're having difficulty."
        />
    );
};

export default NotPermittedScreen;
