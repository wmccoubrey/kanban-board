import React from 'react';

import { observer } from 'mobx-react-lite';

import AuthenticationStore from 'authentication/stores/AuthenticationStore';
import { Redirect } from 'react-router-dom';
import CognitoLoginButton from 'authentication/components/CognitoLoginButton';
import { getCognitoRedirectUrlParams, isCognitoRedirectUrl } from 'authentication/utils/cognito';
import LoginWithSsoPayloadUseCase from 'authentication/use-cases/LoginWithSsoPayloadUseCase';

const LoginScreen = observer(() => {
    const store = AuthenticationStore.get();

    if (!store.isLoading && isCognitoRedirectUrl()) {
        const params = getCognitoRedirectUrlParams();
        LoginWithSsoPayloadUseCase.create().execute(params);
    }

    return (
        <>
            {store.isLoggedIn && <Redirect to="/notes" />}
            <h1>Welcome</h1>
            {!store.isLoading ? <CognitoLoginButton /> : <p>Logging in</p>}
            {store.authenticationErrorMessage && (
                <div className="c-alert u-fill-neg">
                    <p>{store.authenticationErrorMessage}</p>
                </div>
            )}
        </>
    );
});

export default LoginScreen;
