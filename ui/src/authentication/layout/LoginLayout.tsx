import * as React from "react";

const LoginLayout: React.FC = (props) => {
    return (
        <div className="u-flex u-align-items-center u-justify-content-center u-height-100vh u-pad u-align-center u-fill-3">
            <div className="s-cms-content o-wrap u-width-4">{props.children}</div>
        </div>
    );
};

export default LoginLayout;
