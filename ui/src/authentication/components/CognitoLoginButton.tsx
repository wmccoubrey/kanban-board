import InitiateSsoRedirectLoginUseCase from "authentication/use-cases/InitiateSsoRedirectLoginUseCase";
import { Button } from "morse-react";
import * as React from "react";

const CognitoLoginButton: React.FC = () => {
    const handleClick = () => {
        InitiateSsoRedirectLoginUseCase.create().execute();
    };

    return <Button className="qa-SignInWithCognito" onClick={handleClick}>Sign In</Button>;
};

export default CognitoLoginButton;
