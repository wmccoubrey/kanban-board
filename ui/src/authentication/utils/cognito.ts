export const isCognitoRedirectUrl = (): boolean => {
    const urlParams = new URLSearchParams(window.location.hash.replace(/^#/, ''));

    return !!(
        urlParams.get("id_token") &&
        urlParams.get("expires_in") &&
        urlParams.get("token_type")
    );
};

type CognitoRedirectUrlParams = { idToken: string; expiresIn: number; tokenType: string };

export const getCognitoRedirectUrlParams = (): CognitoRedirectUrlParams | null => {
    const urlParams = new URLSearchParams(window.location.hash.replace(/^#/, ''));

    const idToken = urlParams.get("id_token");
    const expiresIn = urlParams.get("expires_in");
    const tokenType = urlParams.get("token_type");

    if (!idToken || !expiresIn || !tokenType) {
        return null;
    }

    return { idToken, expiresIn: parseInt(expiresIn), tokenType };
};
