import UserService from "users/services/UserService";
import AuthenticationStore from "authentication/stores/AuthenticationStore";
import { inject } from "inversify";
import { OrangeUseCase } from "@gcdtech/acorn-react-core";
import { UserAndPermissions } from "users/types/UserAndPermissions";
import { USER_TYPES } from "users/ioc/UserTypes";
import LogoutUseCase from "authentication/logout/use-cases/LogoutUseCase";

export default class DoPostLoginSetupUseCase extends OrangeUseCase {
    @inject(USER_TYPES.UserService) private userService: UserService;

    public *onExecute() {
        const authenticationStore = AuthenticationStore.get();

        if (!authenticationStore.isLoggedIn) {
            return;
        }

        authenticationStore.loadingTimer.startLoading();

        try {
            // Fetch the authenticated User
            const userAndPermissions: UserAndPermissions = yield this.userService.getAuthenticatedUserAndPermissions();

            authenticationStore.authenticatedUser = userAndPermissions.item;
            authenticationStore.permissions = userAndPermissions.permissions;

            authenticationStore.authenticationErrorMessage = undefined;
        } catch (error) {
            yield LogoutUseCase.create().execute();
            authenticationStore.authenticationErrorMessage = error.message;
        }

        authenticationStore.loadingTimer.finishLoading();
    }
}
