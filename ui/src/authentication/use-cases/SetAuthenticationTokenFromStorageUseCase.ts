import { inject, injectable } from "inversify";
import AuthenticationStore from "authentication/stores/AuthenticationStore";
import AuthenticationToken from "authentication/entities/AuthenticationToken";
import { AUTH_TYPES } from "authentication/ioc/AuthenticationTypes";
import { ApiAdapter, CORE_TYPES, OrangeUseCase, StorageService } from "@gcdtech/acorn-react-core";

@injectable()
export default class SetAuthenticationTokenFromStorageUseCase extends OrangeUseCase {
    @inject(CORE_TYPES.StorageService) private storageService: StorageService;
    @inject(CORE_TYPES.ApiAdapter) private apiAdapter: ApiAdapter;
    @inject(AUTH_TYPES.TokenStorageKey) private tokenStorageKey: string;

    // eslint-disable-next-line
    public *onExecute() {
        const tokenFromStorage = this.storageService.getItem(this.tokenStorageKey);

        if (tokenFromStorage === null) {
            return;
        }

        this.apiAdapter.setAuthenticationToken(tokenFromStorage);

        const newToken = new AuthenticationToken();
        newToken.token = tokenFromStorage;

        AuthenticationStore.get().token = newToken;
    }
}
