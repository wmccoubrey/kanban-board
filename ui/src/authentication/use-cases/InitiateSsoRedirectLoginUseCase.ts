import { OrangeUseCase } from "@gcdtech/acorn-react-core";
import { AUTH_TYPES } from "authentication/ioc/AuthenticationTypes";
import LoginStore from "authentication/stores/LoginStore";
import { inject } from "inversify";
import AuthenticationService from "authentication/services/AuthenticationService";

export default class InitiateSsoRedirectLoginUseCase extends OrangeUseCase {
    @inject(AUTH_TYPES.AuthenticationService) private authenticationService: AuthenticationService;

    public *onExecute() {
        const loginStore = LoginStore.get();
        loginStore.error = undefined;
        loginStore.loadingTimer.startLoading();

        try {
            window.location.href = yield this.authenticationService.getLoginUrl();
        } catch (e) {
            if (e.indexOf("Not Implemented") !== -1) {
                loginStore.error =
                    "Login method not supported by given provider. Open `app.module.ts` on the API to configure the correct provider";
                return;
            }

            loginStore.error = e + ". Please try again";
        } finally {
            loginStore.loadingTimer.finishLoading();
        }
    }
}
