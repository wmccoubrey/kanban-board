import AuthenticationStore from "authentication/stores/AuthenticationStore";
import { inject } from "inversify";
import { AUTH_TYPES } from "authentication/ioc/AuthenticationTypes";
import AuthenticationService from "authentication/services/AuthenticationService";
import SetAuthenticationTokenFromStorageUseCase from "./SetAuthenticationTokenFromStorageUseCase";
import DoPostLoginSetupUseCase from "./DoPostLoginSetupUseCase";
import { CORE_TYPES, OrangeUseCase, StorageService } from "@gcdtech/acorn-react-core";

export default class LoginWithSsoPayloadUseCase extends OrangeUseCase {
    @inject(AUTH_TYPES.AuthenticationService) private authenticationService: AuthenticationService;
    @inject(CORE_TYPES.StorageService) private storageService: StorageService;
    @inject(AUTH_TYPES.TokenStorageKey) private tokenStorageKey: string;

    *onExecute(payload: unknown) {
        const authenticationStore = AuthenticationStore.get();

        authenticationStore.loadingTimer.startLoading();

        try {
            // Attempt login with the authentication service
            const token = yield this.authenticationService.login(payload);
            authenticationStore.token = token;

            // If successful, save the token with the StorageService
            this.storageService.saveItem(this.tokenStorageKey, token.token);
            SetAuthenticationTokenFromStorageUseCase.create().execute();

            authenticationStore.authenticationErrorMessage = undefined;

            // Perform post login actions
            yield DoPostLoginSetupUseCase.create().execute();
        } catch (error) {
            authenticationStore.authenticationErrorMessage = error?.response?.data?.message || error.message;
        }

        authenticationStore.loadingTimer.finishLoading();
    }
}
