import React, { useEffect } from "react";

import LogoutUseCase from "authentication/logout/use-cases/LogoutUseCase";

const LogoutScreen = () => {
    // Execute logout use case exactly once.
    useEffect(() => {
        LogoutUseCase.create().execute();
    }, []);

    return (
        <>
            <h1>
                You're now logged out{" "}
                <span role="img" aria-label="waving">
                    👋🏻
                </span>
            </h1>
            <p>Have a nice day</p>
            {/* An anchor rather than a link because it means there's nothing
            the user can do without having to first reload the page, which
            will reset the entire application state. */}
            <a className="c-button" href="/">
                Go home
            </a>
        </>
    );
};

export default LogoutScreen;
