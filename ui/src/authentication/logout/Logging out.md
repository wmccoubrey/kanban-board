# What it does

The `LogoutUseCase` covers everything that needs to happen to ensure that any
further actions made by the user do not result in authenticated requests being
made to the backend.

It currently:

-   Sets the token in the `AuthenticationStore` to `undefined`.
-   Unsets the global header in the `ApiAdapter`.

# When it is executed automatically

The `LogoutUseCase` is executed by the API adapter when there is an issue with
the last request that it made. This is usually down to something like the token
expiring.

# Using in the UI

If you were to tie up the `LogoutUseCase` to a button, you would also need to
ensure that the user is redirected to the logout screen using a `Redirect` or
some other appropriate component is displayed.
