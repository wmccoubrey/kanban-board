import { ApiAdapter, CORE_TYPES, OrangeUseCase, StorageService } from "@gcdtech/acorn-react-core";
import { AUTH_TYPES } from "authentication/ioc/AuthenticationTypes";
import AuthenticationStore from "authentication/stores/AuthenticationStore";
import { inject } from "inversify";

export default class LogoutUseCase extends OrangeUseCase {
    @inject(CORE_TYPES.StorageService) private storageService: StorageService;
    @inject(AUTH_TYPES.TokenStorageKey) private tokenStorageKey: string;
    @inject(CORE_TYPES.ApiAdapter) private apiAdapter: ApiAdapter;

    public *onExecute(message?: string) {
        const authenticationStore = AuthenticationStore.get();

        authenticationStore.token = undefined;
        authenticationStore.authenticatedUser = undefined;

        yield this.apiAdapter.clearAuthenticationToken();

        this.storageService.removeItem(this.tokenStorageKey);

        if (message) {
            authenticationStore.authenticationErrorMessage = message;
        }
    }
}
