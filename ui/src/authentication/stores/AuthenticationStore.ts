import { computed, makeObservable, observable } from "mobx";
import { injectable } from "inversify";
import AuthenticationToken from "authentication/entities/AuthenticationToken";
import { LoadableStore } from "@gcdtech/acorn-react-core";
import UserEntity from "users/entities/UserEntity";

@injectable()
export default class AuthenticationStore extends LoadableStore {
    token?: AuthenticationToken;

    authenticatedUser?: UserEntity;
    authenticationErrorMessage?: string;

    permissions: string[] = [];

    get isLoggedIn() {
        return !!this.token && this.token.token !== "";
    }

    constructor() {
        super();

        makeObservable(this, {
            token: observable,
            authenticatedUser: observable,
            authenticationErrorMessage: observable,
            permissions: observable,
            isLoggedIn: computed,
        });
    }
}
