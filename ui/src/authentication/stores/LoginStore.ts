import { makeObservable, observable } from 'mobx';
import { injectable } from 'inversify';
import { LoadableStore } from '@gcdtech/acorn-react-core';

@injectable()
export default class LoginStore extends LoadableStore {
    constructor() {
        super();

        makeObservable(this, {
            error: observable,
        });
    }

    error?: Error | string;
}
