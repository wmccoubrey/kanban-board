import { createTestContainer } from "@gcdtech/acorn-react-core/build/ioc/testContainer";
import AuthenticationStore from "./AuthenticationStore";
import AuthenticationToken from "authentication/entities/AuthenticationToken";

let authStore: AuthenticationStore;

beforeEach(() => {
    createTestContainer();
    authStore = AuthenticationStore.get();
});

it("returns false if token is undefined", () => {
    authStore.token = undefined;

    expect(authStore.isLoggedIn).toBe(false);
});

it("returns false if token is empty", () => {
    authStore.token = new AuthenticationToken();
    authStore.token.token = "";

    expect(authStore.isLoggedIn).toBe(false);
});

it("returns true if token is set", () => {
    authStore.token = new AuthenticationToken();

    expect(authStore.isLoggedIn).toBe(true);
});
