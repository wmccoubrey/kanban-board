import 'reflect-metadata';
import { configure } from 'mobx';
import {
    ApiAdapter,
    CORE_TYPES,
    getCoreContainer,
    AxiosApiAdapter,
    BrowserLocalStorageService,
} from '@gcdtech/acorn-react-core';
import { StyleModule } from '@gcdtech/acorn-react-styleguide';

import AuthenticationModule from 'authentication/ioc/AuthenticationModule';
import UserModule from 'users/ioc/UserModule';
import BoardModule from './boards/BoardModule';

// Initialise application
configure({ enforceActions: 'observed' });
getCoreContainer().bind(CORE_TYPES.StorageService).to(BrowserLocalStorageService);
getCoreContainer().bind(CORE_TYPES.ApiAdapter).toConstantValue(new AxiosApiAdapter());
getCoreContainer().get<ApiAdapter>(CORE_TYPES.ApiAdapter).init(process.env.REACT_APP_API_URL);

const container = getCoreContainer();
container.load(new StyleModule());

container.load(new AuthenticationModule());
container.load(new UserModule());
container.load(new BoardModule());
