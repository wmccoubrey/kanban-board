import React from 'react';

import { observer } from 'mobx-react-lite';

type SecondaryNavLayoutProps = React.PropsWithChildren<{
  navArea: React.ReactNode;
  actionsArea?: React.ReactNode;
}>;

const SecondaryNavLayout: React.FC<SecondaryNavLayoutProps> = observer((props: SecondaryNavLayoutProps) => {
  return (
    <div className="l-app +y +gap-0">
      <div className="l-app__item +start u-fill-1">
        <div className="l-app +y +x@m u-fill-shade">
          <div className="l-app__item +start u-pad-small u-color-white-all">{props.navArea}</div>
          <div className="l-app__item +end u-flex u-align-items-center u-marg-right">{props.actionsArea}</div>
        </div>
      </div>
      <div className="l-app__item +middle">{props.children}</div>
    </div>
  );
});

export default SecondaryNavLayout;
