import React from "react";
import { observer } from "mobx-react-lite";
import { Link, NavLink, useLocation } from "react-router-dom";
import { useNavItems } from "@gcdtech/acorn-react-core";
import SecondaryNavLayout from "./SecondaryNavLayout";

import { AuthenticatedUserNavArea } from "users/components/AuthenticatedUserNavArea";


const StandardLayout: React.FC = observer((props) => {
    const { pathname } = useLocation();

    const { allowedNavItems } = useNavItems({ userIsAuthenticated: false });
    const activeNavItem = allowedNavItems.find((d) => pathname.startsWith(d.to));
    const primaryNavBarLinks = allowedNavItems.map(({ to, text }) => (
        <NavLink
            key={to}
            to={to}
            isActive={(_, { pathname }) => pathname.startsWith(to)}
            className="u-pad"
            activeClassName="u-fill-shade u-font-bold"
        >
            {text}
        </NavLink>
    ));

    const subNavBarLinks =
        activeNavItem && activeNavItem.subNavItems
            ? activeNavItem.subNavItems?.map(({ to, text }) => (
                  <NavLink
                      key={to}
                      to={to}
                      isActive={(_, { pathname }) => pathname.startsWith(to)}
                      exact
                      className="u-pad-left-right u-pad-small u-marg-right"
                      activeClassName="u-fill-shade u-rounded u-font-bold"
                  >
                      {text}
                  </NavLink>
              ))
            : [];

    const showSubNav =
        (subNavBarLinks && subNavBarLinks.length > 0) ||
        (activeNavItem && activeNavItem.actions && activeNavItem.actions.length > 0);

    return (
        <div id="standard-layout-container" className="l-app +y u-height-100vh +gap-0">
            <header className="l-app__item +start l-app +y +x@m u-pad-left-right-large@m u-fill-1 u-color-white-all">
                <Link
                    to="/"
                    className="l-app__item +start u-flex u-align-items-center u-pad u-pad-none@m c-heading +h1 u-font-xl!"
                >
                    acorn<span className="u-font-regular">.scaffold</span>
                </Link>
                <nav className="l-app__item +middle c-nav-toggle">
                    <ul className="c-nav +inline" style={{ justifyContent: "start" }}>
                        {primaryNavBarLinks.map((link, key) => (
                            <li key={key}>{link}</li>
                        ))}
                    </ul>
                </nav>
                
                <div className="l-app__item +end c-nav-toggle u-flex u-align-items-center">
                    <AuthenticatedUserNavArea />
                </div>
                
            </header>
            <div className="l-app__item +middle">
                {!showSubNav && props.children}
                {showSubNav && (
                    <SecondaryNavLayout
                        navArea={
                            subNavBarLinks ? (
                                <nav className="c-nav +inline">{subNavBarLinks}</nav>
                            ) : undefined
                        }
                        actionsArea={
                            activeNavItem?.actions ? (
                                <>
                                    {activeNavItem.actions.map((Component, i) => (
                                        <Component key={i} />
                                    ))}
                                </>
                            ) : undefined
                        }
                    >
                        {props.children}
                    </SecondaryNavLayout>
                )}
            </div>
        </div>
    );
});

export default StandardLayout;
