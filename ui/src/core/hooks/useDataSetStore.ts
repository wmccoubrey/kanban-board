import { DataSetCriteria } from "@gcdtech/acorn-core";
import { DataSetStore } from "@gcdtech/acorn-react-core";
import { useEffect } from "react";

type UseDataSetStoreConfig<T> = {
    store: DataSetStore<T>;
    initialCriteria: DataSetCriteria<T>;
    destroyOnUnmount?: boolean;
};

// Could potentially be moved into acorn
export const useDataSetStore = <T>(config: UseDataSetStoreConfig<T>) => {
    // Function that can be used to redo the initial fetch
    const initialFetch = () => {
        config.store.fetchList(config.initialCriteria);
    };

    // Refresh the list upon entry
    useEffect(() => {
        if (!config.store.isLoading && !config.store.error) {
            config.store.fetchList(config.store.listCriteria);
        }
    }, [config.store]);

    // If desired, destroy the form on unmount to free up memory
    useEffect(() => {
        if (config.destroyOnUnmount) {
            // @ts-ignore TS doesn't like getting the constructor from the instance
            return () => config.store.constructor.destroy();
        }
    }, [config.store, config.destroyOnUnmount]);

    /**
     * Perform an initial data fetch if:
     *  - Initial fetch hasn't already taken place
     *  - Initial fetch isn't already taking place
     *  - There isn't some other error in the store
     */
    if (!config.store.list && !config.store.isLoading && !config.store.error) {
        initialFetch();
    }

    return {
        initialFetch,
    };
};
