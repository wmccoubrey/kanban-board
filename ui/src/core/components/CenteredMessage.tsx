import React, { PropsWithChildren } from 'react';
import { observer } from 'mobx-react-lite';

export type CenteredMessageProps = {
    icon?: string;
    bigMessage?: string;
    smallMessage?: string;
    showLoader?: boolean;
};

const CenteredMessage: React.FC<PropsWithChildren<CenteredMessageProps>> = observer(
    ({ icon, bigMessage, smallMessage, showLoader, children }) => {
        return (
            <div className="u-flex u-align-items-center u-justify-content-center u-height-100vh u-max-height-100pc">
                <div className="s-cms-content u-align-center">
                    {icon && <p className="c-heading +h1">{icon}</p>}
                    {bigMessage && <p className="u-font-xxl">{bigMessage}</p>}
                    {smallMessage && <p className="u-font-xl">{smallMessage}</p>}
                    {showLoader && (
                        <div className="u-pad-top u-flex u-justify-content-center">
                            <div className="c-loader u-font-xxl"></div>
                        </div>
                    )}
                    {children}
                </div>
            </div>
        );
    },
);

export default CenteredMessage;
