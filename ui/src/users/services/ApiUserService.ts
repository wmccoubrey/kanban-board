import { inject, injectable } from "inversify";
import UserEntity from "../entities/UserEntity";
import UserService from "./UserService";
import { ApiAdapter, ApiThrowsError, CORE_TYPES, ServiceWithApi } from "@gcdtech/acorn-react-core";
import { createNewEntityFromApiData } from "@gcdtech/acorn-react-core/build/services/ApiField";
import { UserAndPermissions } from "../types/UserAndPermissions";

@injectable()
export default class ApiUserService extends ServiceWithApi<UserEntity> implements UserService {
    getGenericConstructor(): new () => UserEntity {
        return UserEntity;
    }

    protected endpointUrl = "/users";
    @inject(CORE_TYPES.ApiAdapter) protected apiAdapter: ApiAdapter;

    @ApiThrowsError
    async getAuthenticatedUserAndPermissions(): Promise<UserAndPermissions> {
        const url = this.endpointUrl + "/me";

        const response = await this.apiAdapter.get<UserAndPermissions>(url);

        return {
            item: createNewEntityFromApiData(UserEntity, response.data.item),
            permissions: response.data.permissions,
        };
    }

    @ApiThrowsError
    async resetPassword(id: string): Promise<{ isValid: boolean; error: string }> {
        try {
            await this.apiAdapter.post(`${this.endpointUrl}/reset-password/${id}`);
            return { isValid: true, error: "" };
        } catch (error) {
            return { isValid: false, error: error.response.data.errors.email };
        }
    }
}
