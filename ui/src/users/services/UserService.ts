import UserEntity from "../entities/UserEntity";
import { DataSetService } from "@gcdtech/acorn-react-core";
import { CrudService } from "@gcdtech/acorn-react-core";

type UserAndPermissions = {
    item: UserEntity;
    permissions: string[];
};

export default interface UserService extends DataSetService<UserEntity>, CrudService<UserEntity> {
    getAuthenticatedUserAndPermissions(): UserAndPermissions | Promise<UserAndPermissions>;
    resetPassword(id: string): Promise<{ isValid: boolean; error: string }>;
}
