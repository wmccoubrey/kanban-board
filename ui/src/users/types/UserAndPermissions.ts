import UserEntity from "../entities/UserEntity";

export type UserAndPermissions = {
    item: UserEntity;
    permissions: string[];
};
