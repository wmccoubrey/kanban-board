import { DataSet } from "@gcdtech/acorn-core";
import UserEntity from "../entities/UserEntity";

export type UsersType = {
    items: DataSet<UserEntity>;
};
