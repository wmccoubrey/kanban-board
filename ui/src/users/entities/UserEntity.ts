import { ApiEntity, ApiField } from "@gcdtech/acorn-react-core";

export default class UserEntity extends ApiEntity<string> {
    @ApiField() enabled: boolean;
    @ApiField() email: string;
    @ApiField() firstName: string;
    @ApiField() lastName: string;
}
