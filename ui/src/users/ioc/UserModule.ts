import { ContainerModule } from "inversify";
import UserService from "../services/UserService";
import ApiUserService from "../services/ApiUserService";
import { USER_TYPES } from "./UserTypes";

export default class UserModule extends ContainerModule {
    constructor() {
        super((bind) => {
            bind<UserService>(USER_TYPES.UserService).to(ApiUserService);
        });
    }
}

export { USER_TYPES };
