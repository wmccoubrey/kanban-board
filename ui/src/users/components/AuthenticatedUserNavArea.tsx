import React from "react";
import { observer } from "mobx-react-lite";
import { useState } from "react";
import AuthenticationStore from "authentication/stores/AuthenticationStore";
import { Link } from "react-router-dom";
import { Panel } from "morse-react";
import { Avatar } from "./Avatar";

export const AuthenticatedUserNavArea = observer(() => {
    const [panelAnchor, setPanelAnchor] = useState<HTMLDivElement | null>(null);
    const [isOpen, setIsOpen] = useState(false);

    const { authenticatedUser } = AuthenticationStore.get();

    return (
        <>
            <div
                onClick={(e: React.MouseEvent<HTMLDivElement>) => {
                    setIsOpen(!isOpen);
                    setPanelAnchor(e.currentTarget);
                }}
                id="qa-UsersPanelToggle"
                className="u-border u-rounded"
                style={{ borderColor: "black", cursor: "pointer" }}
            >
                <Avatar user={authenticatedUser} size={40} />
            </div>

            <Panel
                id="account-options"
                isOpen={isOpen}
                anchorElement={panelAnchor}
                onRequestClose={() => {
                    setIsOpen(false);
                    setPanelAnchor(null);
                }}
            >
                <div className="c-card u-width-1 u-fill-white u-rounded u-overflow-hide u-shadow u-z-top">
                    <div className="c-card__content u-pad u-flex u-align-items-center">
                        <div className="c-card__image">
                            <div className="u-pad-bottom">
                                <Avatar user={authenticatedUser} />
                            </div>
                        </div>
                        <div className="c-card__detail u-dark u-align-center">
                            <p className="u-font-large u-pad-bottom">
                                {authenticatedUser
                                    ? `${authenticatedUser.firstName} ${authenticatedUser.lastName}`
                                    : "Not signed in"}
                            </p>

                            <p className="u-font-medium u-pad-bottom">
                                {authenticatedUser ? authenticatedUser.email : "Not signed in"}
                            </p>
                            <hr />

                            {authenticatedUser ? (
                                <Link to="/auth/logout" className="c-button +long +neg">
                                    Logout
                                </Link>
                            ) : (
                                <Link to="/auth/login" className="c-button +pos">
                                    Login
                                </Link>
                            )}
                        </div>
                    </div>
                </div>
            </Panel>
        </>
    );
});
