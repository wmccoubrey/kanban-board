import { observer } from "mobx-react-lite";
import React from "react";
import UserEntity from "users/entities/UserEntity";

type AvatarProps = {
    user?: UserEntity;
    size?: number;
};

export const Avatar: React.FC<AvatarProps> = observer(({ user, size }) => {
    return (
        <div
            className="c-image +avatar u-border u-flex u-justify-content-center u-align-items-center u-fill-site u-color-text"
            style={{
                width: size,
                height: size,
            }}
        >
            <span className="u-font-bold" style={{ fontSize: (size ?? 48) / 3 }}>
                {user?.firstName}
            </span>
        </div>
    );
});
