/**
 * Roles are a means of grouping permissions into sets of actions that can be
 * assigned to a user to grant them access to certain areas/functionality.
 *
 * You should only check a user's role in exceptional circumstances, otherwise
 * you will want to be checking against a specific permission.
 */
export enum Role {
    Admin = "admin",
    User = "user"
}