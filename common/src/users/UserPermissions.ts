import { Role } from "../Role";

export enum UserPermission {
    List = "UserPermission.List",
    Manage = "UserPermission.Manage",
    Delete = "UserPermission.Delete",
    ResetPassword = "UserPermission.ResetPassword",
}

export const ticketPermissionFormatMap: Record<UserPermission, string> = {
    [UserPermission.List]: "View all Users",
    [UserPermission.Manage]: "Amend the details of an User",
    [UserPermission.Delete]: "Delete Users",
    [UserPermission.ResetPassword]: "Reset User Password",
};

export const userPermissionMap: Record<Role, UserPermission[]> = {
    [Role.Admin]: [...Object.values(UserPermission)],
    [Role.User]: [UserPermission.List],
};
