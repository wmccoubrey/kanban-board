import { Role } from "./Role";
import { userPermissionMap } from "./users/UserPermissions";

export const RolePermissionMap: Record<Role, string[]> = {
    [Role.Admin]: [
        ...userPermissionMap[Role.Admin]
    ],
    [Role.User]: [
        ...userPermissionMap[Role.User]
    ]
};
