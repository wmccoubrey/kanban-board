import { NestFactory } from '@nestjs/core';
import { useContainer } from 'class-validator';
import { DevModule } from './dev.module';
import { ProdModule } from './prod.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { join } from 'path';

async function bootstrap() {
  const Module = process.env.NODE_ENV === 'production' ? ProdModule : DevModule;
  const app = await NestFactory.create(Module);
  useContainer(app.select(Module), { fallbackOnErrors: true });

  app.setGlobalPrefix('/api');

  app.enableCors();

  const swaggerOptions = new DocumentBuilder()
    .setTitle('API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api', app, document);

  // The below call to useStaticAssets uses the underlying express instance to serve static build files from the ui folder
  // This is primarily used in production environment.

  // @ts-ignore
  app.useStaticAssets(join(__dirname, '..', '..', 'ui', 'build'));

  await app.listen(5001);
}

bootstrap();
