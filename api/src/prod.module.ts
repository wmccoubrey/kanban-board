import {Global, Module} from '@nestjs/common';
import {AppModule} from "./app.module";
import {SeederModule} from "@gcdtech/acorn-nest-seeder";

@Global()
@Module({
  imports: [
      AppModule,
      SeederModule
  ],
})
export class ProdModule {
}
