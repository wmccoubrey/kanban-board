import { Inject, Injectable } from "@nestjs/common";
import { Scenario } from "@gcdtech/acorn-nest-seeder";
import BoardFactory from "./board.factory";
import BoardRecipe, { BOARD_DEFAULT } from "./board.recipe";
const SUITE = "boards";

@Injectable()
export default class BoardSeeder {
    @Inject(BoardFactory) factory: BoardFactory;

    @Scenario({ suite: SUITE, name: "Seed a single simple board" })
    public async seedBoard() {
        await this.factory.createBoard(new BoardRecipe().withName(BOARD_DEFAULT).withDescription("This is the board for project orange"));
    }
}