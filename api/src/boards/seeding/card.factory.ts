import { EntityFactory } from '@gcdtech/acorn-nest-seeder';
import SwimlaneRecipe from './swimlane.recipe';
import { Inject, Injectable } from '@nestjs/common';
import SwimlaneFactory from './swimlane.factory';
import CardRecipe from './card.recipe';
import CardEntity from '../entities/card.entity';

@Injectable()
export default class CardFactory extends EntityFactory<CardEntity> {
  @Inject(SwimlaneFactory) laneFactory: SwimlaneFactory;

  getEntityConstructor(): { new (): CardEntity } {
    return CardEntity;
  }

  async createCard(card: CardRecipe) {
    if (!card.title) {
      card.withTitle(this.faker.random.words());
    }

    if (!card.lane) {
      card.withLane(new SwimlaneRecipe());
    }

    if (card.lane instanceof SwimlaneRecipe) {
      card.lane = await this.laneFactory.createSwimlane(card.lane);
    }

    return this.findOrCreateEntity(
      { title: card.title },
      {
        title: card.title,
        laneId: card.lane.id,
        boardId: card.lane.boardId,
      },
    );
  }
}
