import { EntityFactory } from "@gcdtech/acorn-nest-seeder";
import BoardEntity from "../entities/board.entity";
import BoardRecipe, { BOARD_DEFAULT } from "./board.recipe";
import { Injectable } from "@nestjs/common";

@Injectable()
export default class BoardFactory extends EntityFactory<BoardEntity>{
    getEntityConstructor(): { new(): BoardEntity } {
        return BoardEntity;
    }

    createBoard(recipe: BoardRecipe) {
        if (!recipe.name) {
            recipe.withName(BOARD_DEFAULT)
        }

        if (!recipe.description) {
            recipe.withDescription(this.faker.random.words())
        }

        return this.findOrCreateEntity({name: recipe.name}, {name: recipe.name, description: recipe.description}, true)
    }
}