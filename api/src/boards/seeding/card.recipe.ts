import SwimlaneRecipe from './swimlane.recipe';
import SwimlaneEntity from '../entities/swimlane.entity';

export default class CardRecipe {
  title: string;
  lane: SwimlaneRecipe | SwimlaneEntity;

  withTitle(title: string) {
    this.title = title;
    return this;
  }

  withLane(laneRecipe: SwimlaneRecipe | SwimlaneEntity) {
    this.lane = laneRecipe;
    return this;
  }
}
