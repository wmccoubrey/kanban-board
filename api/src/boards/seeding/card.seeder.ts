import SwimlaneRecipe from './swimlane.recipe';
import { Inject, Injectable } from '@nestjs/common';
import { Scenario } from '@gcdtech/acorn-nest-seeder';
import CardFactory from './card.factory';
import CardRecipe from './card.recipe';
import SwimlaneFactory from './swimlane.factory';
import BoardRecipe from './board.recipe';

const SUITE = 'cards';

@Injectable()
export default class CardSeeder {
  @Inject(SwimlaneFactory) laneFactory: SwimlaneFactory;
  @Inject(CardFactory) cardFactory: CardFactory;

  @Scenario({
    suite: SUITE,
    name: 'Board with 3 swimlanes to fit on a single page',
  })
  public async seedCardsForASmallSprint() {
    // Create 3 basic swimlanes
    const openLane = await this.laneFactory.createSwimlane(
      new SwimlaneRecipe().withName('Open'),
    );
    const inProgressLane = await this.laneFactory.createSwimlane(
      new SwimlaneRecipe().withName('In-Progress'),
    );
    const closedLane = await this.laneFactory.createSwimlane(
      new SwimlaneRecipe().withName('Closed'),
    );

    // Create 6 cards, one in each lane
    await this.cardFactory.createCard(new CardRecipe().withLane(openLane));
    await this.cardFactory.createCard(new CardRecipe().withLane(openLane));
    await this.cardFactory.createCard(new CardRecipe().withLane(closedLane));
    await this.cardFactory.createCard(new CardRecipe().withLane(closedLane));
    await this.cardFactory.createCard(
      new CardRecipe().withLane(inProgressLane),
    );
    await this.cardFactory.createCard(
      new CardRecipe().withLane(inProgressLane),
    );
  }

  @Scenario({
    suite: SUITE,
    name: 'Board with 10 swimlanes to implement horizontal scrolling',
  })
  public async seedCardsForABoardWithManySwimlanes() {
    const board = new BoardRecipe().withName('Wide Board');

    const swimlanes = [];
    for (let i = 0; i < 10; i++) {
      const lane = await this.laneFactory.createSwimlane(
        new SwimlaneRecipe().withBoard(board),
      );

      swimlanes.push(lane);
    }

    for (let i = 0; i < swimlanes.length; i++) {
      await this.cardFactory.createCard(
        new CardRecipe().withLane(swimlanes[i]),
      );
    }
  }
}
