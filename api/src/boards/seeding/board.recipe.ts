export const BOARD_DEFAULT = "Project Orange";

export default class BoardRecipe {
    name: string;
    description: string;

    withName(name: string):this {
        this.name = name;
        return this;
    }

    withDescription(description: string):this {
        this.description = description;
        return this;
    }
}