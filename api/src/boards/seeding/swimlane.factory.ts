import BoardRecipe from "./board.recipe";
import { EntityFactory } from "@gcdtech/acorn-nest-seeder";
import SwimlaneEntity from "../entities/swimlane.entity";
import SwimlaneRecipe from "./swimlane.recipe";
import BoardFactory from "./board.factory";
import { Inject, Injectable } from "@nestjs/common";

@Injectable()
export default class SwimlaneFactory extends EntityFactory<SwimlaneEntity> {
    @Inject(BoardFactory) boardFactory: BoardFactory;

    getEntityConstructor(): { new(): SwimlaneEntity } {
        return SwimlaneEntity;
    }

    async createSwimlane(lane: SwimlaneRecipe) {
        if (!lane.name) {
            lane.withName(this.faker.random.word())
        }

        if (!lane.board) {
            lane.withBoard(new BoardRecipe())
        }

        const board = await this.boardFactory.createBoard(lane.board);

        return this.findOrCreateEntity({name: lane.name}, {name: lane.name, board: board})
    }
}