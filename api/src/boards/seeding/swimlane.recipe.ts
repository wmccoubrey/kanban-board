import BoardRecipe from "./board.recipe";

export default class SwimlaneRecipe {
    name: string;
    board: BoardRecipe;

    withName(name: string) {
        this.name = name;
        return this;
    }

    withBoard(board: BoardRecipe) {
        this.board = board;
        return this;
    }
}