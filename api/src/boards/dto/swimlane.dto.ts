import {
  FilterableField,
  FilterableRelation,
} from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, Int, ObjectType } from '@nestjs/graphql';
import BoardDto from './board.dto';
import CardDto from './card.dto';

@ObjectType('Swimlane')
@FilterableRelation('board', () => BoardDto, {
  disableRemove: true,
  disableUpdate: true,
})
@FilterableRelation('cards', () => [CardDto], {
  disableRemove: true,
  disableUpdate: true,
  enableAggregate: true,
})
export default class SwimlaneDto {
  @FilterableField(() => Int)
  id: number;

  @FilterableField(() => String)
  name: string;

  @FilterableField(() => Int)
  boardId: number;

  @FilterableField(() => Int)
  order: number;

  @FilterableField(() => GraphQLISODateTime)
  createdDate: Date;

  @FilterableField(() => GraphQLISODateTime)
  updatedDate: Date;
}
