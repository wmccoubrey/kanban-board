import {
  FilterableField,
  FilterableRelation,
} from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, Int, ObjectType } from '@nestjs/graphql';
import BoardDto from './board.dto';
import SwimlaneDto from './swimlane.dto';

@ObjectType('Card')
@FilterableRelation('lane', () => SwimlaneDto, {
  disableRemove: true,
  disableUpdate: true,
})
@FilterableRelation('board', () => BoardDto, {
  disableRemove: true,
  disableUpdate: true,
})
export default class CardDto {
  @FilterableField(() => Int)
  id: number;

  @FilterableField(() => String)
  title: string;

  @FilterableField(() => Int)
  boardId: number;

  @FilterableField(() => Int)
  laneId: number;

  @FilterableField(() => Int)
  order: number;

  @FilterableField(() => GraphQLISODateTime)
  createdDate: Date;

  @FilterableField(() => GraphQLISODateTime)
  updatedDate: Date;
}
