import { Field, GraphQLISODateTime, Int, ObjectType } from '@nestjs/graphql';
import {
  FilterableField,
  FilterableRelation,
} from '@nestjs-query/query-graphql';
import CardDto from './card.dto';
import SwimlaneDto from './swimlane.dto';

@ObjectType('Board')
@FilterableRelation('lanes', () => [SwimlaneDto], {
  disableRemove: true,
  disableUpdate: true,
})
@FilterableRelation('cards', () => [CardDto], {
  disableRemove: true,
  disableUpdate: true,
  enableAggregate: true,
})
export default class BoardDto {
  @FilterableField(() => Int)
  id: number;

  @FilterableField(() => String)
  name: string;

  @FilterableField(() => String)
  description: string;

  @Field(() => GraphQLISODateTime)
  createdDate: Date;

  @Field(() => GraphQLISODateTime)
  updatedDate: Date;
}
