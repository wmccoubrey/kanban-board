import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import BoardEntity from './board.entity';
import CardEntity from './card.entity';

@Entity()
export default class SwimlaneEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  boardId: number;

  @ManyToOne(() => BoardEntity, (b) => b.lanes)
  @JoinColumn({ name: 'boardId' })
  board: BoardEntity;

  @OneToMany(() => CardEntity, (l) => l.lane)
  cards: CardEntity[];

  @Column()
  name: string;

  @Column({ default: 0 })
  @Index()
  order: number;

  @CreateDateColumn({ type: 'datetime' })
  createdDate: Date;

  @UpdateDateColumn({ type: 'datetime' })
  updatedDate: Date;
}
