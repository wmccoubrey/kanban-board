import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
} from 'typeorm';
import BoardEntity from './board.entity';
import SwimlaneEntity from './swimlane.entity';

@Entity({ orderBy: { order: 'ASC' } })
export default class CardEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  boardId: number;

  @ManyToOne(() => BoardEntity, (b) => b.cards)
  @JoinColumn({ name: 'boardId' })
  board: BoardEntity;

  @Column()
  laneId: number;

  @ManyToOne(() => SwimlaneEntity, (b) => b.cards)
  @JoinColumn({ name: 'laneId' })
  lane: SwimlaneEntity;

  @Column()
  title: string;

  @Column({ default: 0 })
  @Index()
  order: number;

  @CreateDateColumn({ type: 'datetime' })
  createdDate: Date;

  @UpdateDateColumn({ type: 'datetime' })
  updatedDate: Date;
}
