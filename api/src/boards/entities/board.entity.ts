import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import SwimlaneEntity from "./swimlane.entity";
import CardEntity from "./card.entity";

@Entity()
export default class BoardEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToMany(() => SwimlaneEntity, l => l.board)
    lanes: SwimlaneEntity[]

    @OneToMany(() => CardEntity, l => l.board)
    cards: CardEntity[]

    @Column()
    name: string;

    @Column()
    description: string;

    @CreateDateColumn({ type: 'datetime' })
    createdDate: Date;

    @UpdateDateColumn({ type: 'datetime' })
    updatedDate: Date;
}
