import { Module } from '@nestjs/common';
import BoardEntity from './entities/board.entity';
import { UserModule } from '@gcdtech/acorn-nest-user';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import BoardDto from './dto/board.dto';
import SwimlaneEntity from './entities/swimlane.entity';
import CardEntity from './entities/card.entity';
import SwimlaneDto from './dto/swimlane.dto';
import CardDto from './dto/card.dto';
import BoardFactory from './seeding/board.factory';
import CardFactory from './seeding/card.factory';
import SwimlaneFactory from './seeding/swimlane.factory';
import BoardSeeder from './seeding/board.seeder';
import CardSeeder from './seeding/card.seeder';

@Module({
  imports: [
    UserModule,
    NestjsQueryGraphQLModule.forFeature({
      imports: [
        NestjsQueryTypeOrmModule.forFeature([
          BoardEntity,
          SwimlaneEntity,
          CardEntity,
        ]),
      ],
      resolvers: [
        {
          DTOClass: BoardDto,
          EntityClass: BoardEntity,
          decorators: [],
          enableAggregate: true,
        },
        {
          DTOClass: SwimlaneDto,
          EntityClass: SwimlaneEntity,
          decorators: [],
          enableAggregate: true,
        },
        {
          DTOClass: CardDto,
          EntityClass: CardEntity,
          decorators: [],
          enableAggregate: true,
        },
      ],
    }),
  ],
  providers: [
    BoardFactory,
    CardFactory,
    SwimlaneFactory,
    BoardSeeder,
    CardSeeder,
  ],
})
export class BoardsModule {}
