import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import {
  DatabaseModule,
  ValidationErrorFilter,
} from '@gcdtech/acorn-nest-core';
import { ValidationError } from 'class-validator';
import {
  AuthGuard,
  PermissionsModule,
  RoleGuard,
  UserModule,
} from '@gcdtech/acorn-nest-user';
import { Role, RolePermissionMap } from 'common';
import { JwtModule } from '@nestjs/jwt';
import { BoardsModule } from './boards/boards.module';
import { GraphQLModule } from '@nestjs/graphql';

@Module({
  imports: [
    {
      ...JwtModule.register({ secret: 'kanban' }),
      global: true,
    },
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `.env.${
        process.env.NODE_ENV ? process.env.NODE_ENV : 'local'
      }`,
    }),
    {
      ...UserModule.register({
        imports: [ConfigModule],
        inject: [ConfigService],
        provider: 'cognito',
        useFactory: (configService: ConfigService) => ({
          accessKeyId: configService.get('AWS_ACCESS_KEY'),
          secretAccessKey: configService.get('AWS_SECRET_KEY'),
          region: configService.get('AWS_REGION'),
          userPoolId: configService.get('AWS_COGNITO_USER_POOL_ID'),
          appClientId: configService.get('AWS_COGNITO_APP_CLIENT_ID'),
          authTokenExpiry: '1 day',
          refreshTokenExpiry: '30 days',
          rolesAndPermissions: RolePermissionMap,
          defaultRoles: [Role.Admin],
          emailIsPrimaryKey: true,
        }),
      }),
      global: true,
    },
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
    }),

    DatabaseModule,
    BoardsModule,
    PermissionsModule.register(RolePermissionMap),
  ],

  providers: [
    { provide: APP_GUARD, useClass: AuthGuard },
    { provide: APP_GUARD, useClass: RoleGuard },

    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({
        whitelist: true,
        transform: true,
        forbidNonWhitelisted: true,
        forbidUnknownValues: true,
        exceptionFactory: (errors) => {
          const error = new ValidationError();
          error.children = errors;

          throw error;
        },
      }),
    },
    { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
    { provide: APP_FILTER, useClass: ValidationErrorFilter },
  ],

  exports: [ConfigModule, DatabaseModule, JwtModule, UserModule],
})
export class AppModule {}
