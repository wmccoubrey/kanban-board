import { Module } from "@nestjs/common";
import { AppModule } from "./app.module";
import { CommandModule } from "nestjs-command";
import { SeederModule } from "@gcdtech/acorn-nest-seeder";

@Module({
    imports: [AppModule, SeederModule, CommandModule],
})
export class CliModule {}
