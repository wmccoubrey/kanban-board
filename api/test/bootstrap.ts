import { NestApplication } from "@nestjs/core";
import { Test } from "@nestjs/testing";
import { AppModule } from "../src/app.module";
import { Connection } from "typeorm";
import { EntityFactory } from "copious/dist";
import { useContainer } from "class-validator";

let app: NestApplication;
let connection: Connection;

export const getApp = (): NestApplication => app;

export const getConnection = (): Connection => connection;

beforeAll(async () => {
    process.env.DB_TYPE = "sqlite";

    const appModule = await Test.createTestingModule({
        imports: [AppModule],
    }).compile();

    app = appModule.createNestApplication();
    await app.init();

    connection = app.get(Connection);
    EntityFactory.setConnection(connection);

    useContainer(getApp().select(AppModule), { fallback: true, fallbackOnErrors: true });
});

beforeEach(async () => {
    await getConnection().synchronize(true);
});
